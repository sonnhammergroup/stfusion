
# STfusion
A fusion transcript lacks a poly(A) tail for the 5´ gene and has an elevated number of poly(A) tails for the 3´ gene. STfusion measures the difference between the observed and expected number of poly(A) tails with a novel C-score. With STfusion and the use of C-scores, fusion transcripts can be spatially localised in clinical tissue sections on almost single cell level.   

Currently, STfusion uses spatial transcriptomics to infer the presence and absence of poly(A) tails. The results of any other method producing the number of Poly(A) tails per gene is possible to use; our method is independent of the platform.



[TOC]

## Installation
git clone https://bitbucket.org/sonnhammergroup/stfusion.git


## Usage
```R
library(STfusion)
stfusion()
```

### Dependencies
STfusion requires the R package ggplot2.


### Example data
An example data set can be found in [STfusion/inst/extdata](inst/extdata/).



## Alternative usage
An interactive web application (Shiny app) of *STfusion*  is freely available at [stfusion.shinyapps.io/stfusion_shiny](https://stfusion.shinyapps.io/stfusion_shiny/)


## Version
Current version is 1.2 .
